package main

import "fmt"
import "net/http"
import "strings"
import "log"

func SayHelloName(w http.ResponseWriter, r *http.Request) {
	r.ParseForm()
	fmt.Println(r.Form)
	fmt.Println("path: ", r.URL.Path)
	fmt.Println("schema: ", r.URL.Scheme)
	fmt.Println("param Long", r.Form["url_long"])
	for k,v := range r.Form {
		fmt.Printf("key: %s, value: %s\n", k, strings.Join(v, ","))
	}

	fmt.Fprintf(w, "hello browser")
}

func main() {
	http.HandleFunc("/hello", SayHelloName)
	err := http.ListenAndServe(":9090", nil)
	if err == nil {
		log.Fatal("ListenAndServe: ", err)
	}
}