package main

import (
        "io/ioutil"
        "fmt"
        "os"
   )

func check(e error) {
    if e != nil {
        fmt.Println("--error--")
        panic(e)
    }
}

func main() {

    data, err := ioutil.ReadFile("/Users/yangshoujun/Desktop/sql.txt");
    check(err)
    fmt.Print(string(data))

    file,err := os.Open("/Users/yangshoujun/Desktop/sql.txt")
    b1 := make([]byte, 20)
    file.Read(b1)
    check(err)
    fmt.Println("---------------------------")
    fmt.Println(string(b1))

    f,err := os.Create("/Users/yangshoujun/Desktop/sql-bak.txt")
    f.Write(b1)
    f.Sync()
    defer f.Close()

    var arr [5]int
    fmt.Println(arr)

    arr = [...]int{1,2,3,4,5}
    fmt.Println(arr)
}