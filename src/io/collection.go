package main

import ("fmt")
func main() {


	dict := map[string]string{"firstName":"yang", "fullName":"yangshoujun"}
	dict["address"] = "广东"
	fmt.Println(dict["fullName"])
	fmt.Println(dict)

	for key,value := range dict {
		fmt.Printf("key:%s,value:%s\n", key, value)
	}

	_,exists := dict["aaaa"]
	fmt.Println(exists)

	sayHi("sky")
}

func sayHi(name string) {
	fmt.Println("hello," +name)
}
