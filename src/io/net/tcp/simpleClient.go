package main

import "net"
import "fmt"
import "time"

func main() {

	conn,err := net.Dial("tcp", ":8080")
	if err != nil {
		panic(err)
	}


	for {

		conn.Write([]byte("hi,server " + time.Now().Format("2006-01-02 15:04:05")))

		buf := make([]byte, 100)
		if _,err := conn.Read(buf); (err == nil) {
			if len(buf) > 0 {
				fmt.Println("接收到数据：" + string(buf))
			}
		}

	}
}
