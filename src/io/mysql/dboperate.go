package main

import "database/sql"
import _ "github.com/go-sql-driver/mysql"
import "fmt"

func check(e error) {
	if e != nil {
		fmt.Println(e)
	}
}

func main() {

	//db, _ := sql.Open("mysql", "root:root@tcp(localhost:3306)/mysql?charset=utf8")
	db, _ := sql.Open("mysql", "root:root@/mysql?charset=utf8")
	rows, _ := db.Query("SELECT host,user,password FROM user")

	columns,_ := rows.Columns()
	scanParams := make([]interface{}, len(columns))
	values := make([]interface{}, len(columns))
	for i := range values {
		scanParams[i] = &values[i]
	}

	for rows.Next() {
		rows.Scan(scanParams...)
		fmt.Println(scanParams)
		record := make(map[string]string)
		for i, col := range values {
			if col != nil {
				record[columns[i]] = string(col.([]byte))
			}
		}
		fmt.Println(record)
	}

	x,y := test("a", "b");

	fmt.Println(x, y)

}

func test(x interface{}, y interface{}) (a string, b string) {

	return x.(string), y.(string)
}
