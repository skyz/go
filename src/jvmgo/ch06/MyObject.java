public class MyObject {
    public static int staticVar;
    public int instanceVar;

    public static void main(String[] args) {
        int x = 32768;
        MyObject myObject = new MyObject();
        myObject.staticVar = x;
        x= myObject.staticVar;
        myObject.instanceVar = x;
        x = myObject.instanceVar;
        Object obj = myObject;
        if(obj instanceof MyObject) {
            System.out.println(myObject.instanceVar);
        }
    }
}