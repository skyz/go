package main
import "fmt"
import "strings"
import "jvmgo/ch02/classpath"

func main() {
	cmd := parseCmd()
	if cmd.versionFlag {
		fmt.Println("version 0.0.1")
	} else if cmd.helpFlag {
		printUsage()
	} else {
		startJVM(cmd)
	}
}

func startJVM(cmd *Cmd) {
	cp := classpath.Parse(cmd.xJreOption, cmd.cpOption)
	fmt.Printf("classpath: %s, class: %s, args: %s\n", cmd.xJreOption, cmd.class, cmd.args)
	className := strings.Replace(cmd.class, ".", "/", -1)
	classData,_,err := cp.ReadClass(className)
	if err != nil {
		fmt.Printf("Could't find or loan main class %s\n", cmd.class)
		return
	} 

	fmt.Printf("class data： %v\n", classData)
}