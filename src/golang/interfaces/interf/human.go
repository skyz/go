package interf

type Human interface {
	SayHi(name string)
}