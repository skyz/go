package interf

import "fmt"

type American struct {

}

func (self *American) SayHi(name string) {
	fmt.Println("Hi," + name)
}
