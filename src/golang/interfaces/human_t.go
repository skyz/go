package main

import (
	"golang/interfaces/interf"
	"fmt"
)

func main()  {
	people := getPerple("cn")
	people.SayHi("杨守军")

	people = getPerple("us")
	people.SayHi("杨守军")

	p, ok := people.(interf.Human)
	fmt.Println(p)
	fmt.Println(ok)
}

func getPerple(contry string) interf.Human {
	if contry == "us" {
		return &interf.American{}
	} else if contry == "cn" {
		return &interf.Chinese{}
	}
	return nil
}