package main

import "fmt"

type Object interface {

}

type Person struct {
	name string
}

type Something struct {

}

func main() {

	fmt.Println(string(9), "|")
	fmt.Println(string("a"))

	//var num1 interface{}
	//
	//var obj Object
	//var p Object
	//var num int
	//fmt.Println(num)
	//
	//obj = &Person{}
	//p = &Something{}
	//
	//num1 = 12
	//num1 = num1.(int)
	//
	//switch num1.(type) {
	//	case int:
	//		fmt.Println("int...")
	//		break
	//}
	//
	//switch obj.(type) {
	//case *Person:
	//	fmt.Println("object...")
	//}
	//
	//switch p.(type) {
	//case *Something:
	//	fmt.Println("person...")
	//}
	//
	//fmt.Println(num1)
}
