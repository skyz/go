package main

import "fmt"

type Rect struct {
	x int
	y int
}

func (r *Rect) Area() int {
	return r.x * r.y
}

func main() {

	rect := new(Rect)
	rect = &Rect{10, 20}
	area := rect.Area()

	fmt.Println(area)
}
