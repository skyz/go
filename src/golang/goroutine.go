package main

import (
	"fmt"
	"time"
)

func main() {

	produceAndConsume()
	time.Sleep(5000)
}

func concurrentAdd() {
	for i:= 0;i <= 100;i++ {
		go counter(i)
	}
	fmt.Println(count)
}

var count  int = 0
func counter(c int) {
	count = count + c
}

func produceAndConsume() {
	var channel chan int
	channel = make(chan int,5)
	go produce(channel)
	go consume(channel)
	time.Sleep(1e9)
}


func produce(channel chan int) {
	for i:= 0;i < 100;i++{
		fmt.Println("produce: ", i)
		channel <- i
	}
}

func consume(channel chan int) {
	for i := 0;i < 100;i++ {
		val := <- channel
		fmt.Println("consume: ", val)
	}
}

