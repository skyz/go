package main

import "fmt"

func main() {

	vName := "shoujun.yang"
	var name *string
	name = &vName
	n2 := *name
	fmt.Println("n2: " + n2)
	p := &Person{name:name, address:"shenzhen"}
	testPerson(p)

	var i int = 8
	var a *int
	a = &i
	test(a)
	testPerson1(*p)
	getPerson()
}

func test(num *int) {
	fmt.Println(*num)
}

type Person struct {
	name *string
	address string
}

func testPerson(p *Person) {
	fmt.Println(&p)
	fmt.Println(*p.name)

	var name *string
	name = p.name
	fmt.Println(*p)
	fmt.Println(*name)
	fmt.Println(&name)
}

func testPerson1(p Person) {
	fmt.Println(*p.name)
	fmt.Println(p.name)
	fmt.Println(p.address)
}

func  getPerson() *Person {
	var tmp string = "sky"
	n := &tmp
	return &Person{name:n, address:"shenzhen"}
}