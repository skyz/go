package people

import "fmt"

type People struct {
	Country string
}

func (p *People) Say(words string) {
	fmt.Println("default " + p.Country + " people say: " + words)
}