package main


import "golang/inheritance/people"

func main() {
	p := &people.Chinese{people.People{Country : "cn"}}
	p.Say("你好")

	p2 := &people.American{people.People{Country : "cn"}}
	p2.Say("hi")
}
